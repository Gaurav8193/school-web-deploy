import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faArrowRight, faEnvelope, faLocationPin, faPhone } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image'
import Form from '@/app/components/form';
export default function AboutUs() {
    return (
        <>
            {/* Header Start */}
            <div className="container-fluid bg-primary py-5 page-header mt-5">
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-10 text-center">
                            <h1 className="display-3 text-white animated slideInDown">About Us</h1>
                        </div>
                    </div>
                </div>
            </div>
            {/* Header End  */}
            

            <div className='container mb-5' style={{ textAlign: "justify", marginTop: "5%" }}>
                <div className="clearfix">
                    <Image src="/feature/module.jpeg" className=" float-md-end mb-3 ms-md-3 img-fluid position-relative" width={640} height={360} alt="..." style={{ width: "auto", height: "auto" }} />

                    <h2 className="text-center mb-3 about-heading" style={{ color: "#ff7a00" }}>Welcome to MithilaSchool.com</h2>
                    <p>At MithilaSchool.com, we are committed to revolutionizing the way schools manage their operations and enhance their educational offerings. Our comprehensive school management system is designed to streamline administrative tasks, facilitate communication, and improve the overall efficiency of educational institutions.</p>

                    <h4>Our Mission</h4>
                    <p>Our mission at MithilaSchool.com is to empower schools with innovative technology solutions that simplify management processes and enhance the educational experience for students, teachers, and administrators. We aim to create a seamless and user-friendly platform that addresses the unique needs of each school.</p>

                    <h4>Our Vision</h4>
                    <p>e envision a future where educational institutions can focus more on teaching and learning, and less on administrative burdens. MithilaSchool.com strives to be at the forefront of educational technology, providing tools that foster collaboration, increase transparency, and drive academic success.</p>

                    <h4>Our Services</h4>
                    <h5>Comprehensive School Management</h5>
                    <p>MithilaSchool.com offers an all-in-one platform that covers various aspects of school management, including student enrollment, attendance tracking, grade management, and report generation. Our system ensures that administrative tasks are handled efficiently, allowing educators to focus on what they do best – teaching.</p>
                    

                    <h5>Enhanced Communication</h5>
                    <p>Effective communication is key to a successful educational environment. MithilaSchool.com facilitates seamless communication between teachers, students, parents, and administrators through integrated messaging, notifications, and announcements. Our platform ensures that everyone stays informed and connected.</p>
                    

                    <h5>Secure Data Management</h5>
                    <p>We prioritize the security and confidentiality of your data. MithilaSchool.com employs robust security measures to protect sensitive information and ensure that data is accessible only to authorized users. Our system is designed to comply with data protection regulations, giving you peace of mind.</p>
                    

                    
                    <h5>Customizable Solutions</h5>
                    <p>We understand that each school has unique requirements. MithilaSchool.com offers customizable solutions that can be tailored to fit the specific needs of your institution. Whether you are a small private school or a large public district, our platform can be adapted to meet your demands.</p>
                    
                    <h4>Why Choose Us?</h4>
                    <h5>User-Friendly Interface</h5>
                    <p>Our intuitive and user-friendly interface ensures that administrators, teachers, and parents can easily navigate and utilize the platform. We provide training and support to ensure that all users can maximize the benefits of our system.</p>

                    <h5>Reliable Support</h5>
                    <p>MithilaSchool.com is committed to providing excellent customer service. Our dedicated support team is always available to assist you with any questions or issues that may arise. We are here to help you make the most of our platform.</p>

                    <h5>Continuous Improvement</h5>
                    <p>We are constantly working to improve and update our system based on feedback from our users and advancements in technology. MithilaSchool.com is dedicated to staying ahead of the curve and providing the best possible solutions for school management.</p>

                    <h4>Contact Us</h4>
                    <p>For more information about MithilaSchool.com and how our school management system can benefit your institution, please visit our website at mithilaschool.com or contact us directly. We look forward to partnering with you to enhance your schools operations and educational experience.</p>
                </div>
            </div>
            {/* About End */}

            {/* ---------------------------------------------------------------------------- */}
      {/* Contact Start */}
      <div className="text-center wow fadeInUp mt-5" data-wow-delay="0.1s">
        <h6 className="section-title bg-white text-center text-color px-3">
          Contact Us
        </h6>
        <h3>Contact for any query</h3>
      </div>
      <div className="container py-3 pb-5">
        <div className="row g-4 justify-content-center">
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  icon={faEnvelope}
                  className="icon-responsive"
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">E-mail</h5>
                <p>mithilaschoolhelp@gmail.com</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  icon={faPhone}
                  className="icon-responsive"
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">Phone</h5>
                <p>+91 7250410621</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.7s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  className="fab fa-whatsapp-in"
                  icon={faWhatsapp}
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">WhatsApp Group</h5>
                <p>
                  <a
                    className="link-dark"
                    href="https://chat.whatsapp.com/DdnuXfUq2lJAiQfJhGQR5H"
                  >
                    Join Now
                  </a>
                </p>
              </div>
            </div>
          </div>
          </div>
          </div>
            <Form></Form>
        </>
    );
}