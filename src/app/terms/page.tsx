export default function Terms() {
  return (
    <>
      <div className="container about-container mt-5">
        <h1 style={{ color: "#ff7a00" }}>Terms of Service</h1>
        <p>
          Welcome to mithilaschool.com. These Terms of Service govern your use of our Website and any related services provided by us.
        </p>

        <h2>Acceptance of Terms</h2>
        <p>
          By accessing or using our Website, you agree to comply with and be bound by these Terms and all applicable laws and regulations. If you do not agree to these Terms, you must discontinue use of our Website immediately.
        </p>

        <h2>Use of the Website</h2>
        <p>
        <strong className="strong-style">a. Eligibility:</strong> You must be at least 18 years old to use our Website. By using our Website, you represent and warrant that you meet this age requirement. 
        <strong className="strong-style">b. User Account:</strong> To access certain features of the Website, you may need to create an account. You agree to provide accurate and complete information and to update your account information as necessary. You are responsible for maintaining the confidentiality of your account credentials and for all activities that occur under your account. 
        <strong className="strong-style">c. Prohibited</strong> Activities: You agree not to engage in any of the following prohibited activities:
        </p>
        <ul>
          <li><p>Violating any applicable laws or regulations.</p></li>
          <li><p>Infringing on the rights of others, including intellectual property rights.</p></li>
          <li><p>Engaging in any activity that could damage, disable, or impair the functionality of the Website.</p></li>
          <li><p>Using automated means to access the Website or collect information from the Website.</p></li>
        </ul>

        <h2>Intellectual Property</h2>
        <p>
        <strong className="strong-style">a. Ownership:</strong> The Website and its content, including but not limited to text, graphics, logos, and software, are the property of the Company or its licensors and are protected by intellectual property laws. 
        <strong className="strong-style">b. License:</strong> We grant you a limited, non-exclusive, non-transferable license to access and use the Website for personal, non-commercial purposes. You may not reproduce, distribute, or create derivative works from the Website without our prior written consent.
        </p>

        <h2>Privacy</h2>
        <p>
        Your use of our Website is also governed by our Privacy Policy, which is incorporated into these Terms by reference. Please review our Privacy Policy to understand how we collect, use, and protect your personal information.
        </p>

        <h2>Disclaimers</h2>
        <p>
        <strong className="strong-style">a. No Warranty:</strong> The Website is provided on an `as-is` and `as-available` basis. We make no warranties or representations about the accuracy or completeness of the Websites content or the content of any sites linked to the Website. 
        <strong className="strong-style">b. Limitation of Liability:</strong> To the fullest extent permitted by law, we disclaim all warranties, express or implied, including but not limited to implied warranties of merchantability, fitness for a particular purpose, and non-infringement. We shall not be liable for any indirect, incidental, special, or consequential damages arising out of or in connection with the use of the Website.
        </p>

        <h2>Indemnification</h2>
        <p>
        You agree to indemnify, defend, and hold harmless the Company, its officers, directors, employees, and agents from and against any claims, liabilities, damages, losses, and expenses, including reasonable attorneys fees, arising out of or in any way connected with your use of the Website or violation of these Terms.
        </p>
        <h2>Modifications to the Terms</h2>
        <p>
        We reserve the right to modify these Terms at any time. Any changes will be effective immediately upon posting on the Website. Your continued use of the Website after the posting of revised Terms constitutes your acceptance of the changes.
        </p>

        <h2>Governing Law</h2>
        <p>
        These Terms shall be governed by and construed in accordance with the laws of India, without regard to its conflict of law principles. Any legal action or proceeding arising out of or relating to these Terms shall be brought exclusively in the federal or state courts located in India.
        </p>

        <h2>Contact Information</h2>
        <p>
        If you have any questions or concerns about these Terms, please contact us at mithilaschoolhelp@gmail.com. By using our Website, you acknowledge that you have read, understood, and agree to be bound by these Terms of Service.
        </p>
      </div>
    </>
  );
}
