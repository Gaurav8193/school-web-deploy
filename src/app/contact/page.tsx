"use client";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import {
  faEnvelope,
  faPhone,
  faLocationPin,
  faArrowRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import Form from '@/app/components/form';

export default function ContactUs() {
  const [yourName, setyourName] = useState("");
  const [email, setemail] = useState("");
  const [subject, setsubjectName] = useState("");
  const [message, setmessage] = useState("");
  const handleSubmit = async (event: { preventDefault: () => void }) => {
    // event.preventDefault();
    console.log(yourName + " " + email + " " + subject + " " + message);

    fetch("https://api.mithilaschool.com/demo", {
      method: "POST",
      body: JSON.stringify({ yourName, email, subject, message }),
      headers: {
        "content-type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept",
      },
    }).catch((e) => console.log(e));
    return alert(
      "Thank you! \nYour message has been successfully sent. We will contact you soon!"
    );
  };
  return (
    <>
      {/* Header Start */}
      <div className="container-fluid bg-primary py-5 page-header mt-5">
        <div className="container py-5">
          <div className="row justify-content-center">
            <div className="col-lg-10 text-center">
              <h1 className="display-3 text-white animated slideInDown">
                Contact
              </h1>
            </div>
          </div>
        </div>
      </div>
      {/* Header End  */}
      {/* Contact Start */}
      <div className="container-xxl py-5">
        <div className="container">
          <div className="text-center">
            <h6 className="section-title bg-white text-center text-color px-3">
              Contact Us
            </h6>
            <h1 className="mb-5" style={{ color: "#ff7a00" }}>
              Contact for any query
            </h1>
          </div>
          <div className="row g-4 justify-content-center">
            <div
              className="col-lg-4 col-md-12 wow fadeInUp"
              data-wow-delay="0.1s"
            >
              <h5>Get In Touch</h5> {/* style={{ color: "#ff7a00" }} */}
              <p className="mb-4">
                For any inquiries or if you need additional information, don’t
                hesitate to reach out to us. Our friendly team is ready to
                assist with your questions and provide you with the answers you
                need. Contact us anytime!
              </p>
              <div className="d-flex align-items-center mb-3">
                <div
                  className="d-flex align-items-center justify-content-center flex-shrink-0 background"
                  style={{ width: "50px", height: "50px", borderRadius: "10px" }}
                >
                  {/* <FontAwesomeIcon
                    icon={faLocationPin}
                    className="text-white"
                    width={40}
                    height={40}
                  /> */
                  <FontAwesomeIcon
                  className="text-white"
                  icon={faWhatsapp}
                  width={25}
                  height={25}
                  color="#06BBCC"
                />}
                </div>
                <div className="ms-3">
                  <h5 className="text-color">WhatsApp Group</h5>
                  <p className="mb-0"><a
                    className="link-dark"
                    href="https://chat.whatsapp.com/DdnuXfUq2lJAiQfJhGQR5H"
                  >
                    Join Now
                  </a></p>
                </div>
                {/* <div className="ms-3">
                  <h5 className="text-color">Office</h5>
                  <p className="mb-0">Chennai India | USA</p>
                </div> */}
              </div>
              <div className="d-flex align-items-center mb-3">
                <div
                  className="d-flex align-items-center justify-content-center flex-shrink-0 background"
                  style={{ width: "50px", height: "50px", borderRadius: "10px" }}
                >
                  <FontAwesomeIcon
                    icon={faPhone}
                    className="text-white"
                    width={40}
                    height={40}
                  />
                </div>
                <div className="ms-3">
                  <h5 className="text-color">Mobile</h5>
                  <p className="mb-0">+91 7250410621</p>
                </div>
              </div>
              <div className="d-flex align-items-center">
                <div
                  className="d-flex align-items-center justify-content-center flex-shrink-0 background"
                  style={{ width: "50px", height: "50px", borderRadius: "10px" }}
                >
                  <FontAwesomeIcon
                    icon={faEnvelope}
                    className="text-white"
                    width={40}
                    height={40}
                  />
                </div>
                <div className="ms-3">
                  <h5 className="text-color">Email</h5>
                  <p className="mb-0">mithilaschoolhelp@gmail.com</p>
                </div>
              </div>
            </div>

            

            <div
              className="col-lg-4 col-md-12 wow fadeInUp"
              data-wow-delay="0.5s"
            >
              <h5 className="text-center">
                Drop Your Message
              </h5>
              <form onSubmit={handleSubmit}>
                <div className="row g-3">
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        id="yourname"
                        className="form-control form-inputbox-bgcolor"
                        name="yourname1"
                        value={yourName}
                        onChange={(e) => setyourName(e.target.value)}
                        required
                      />
                      <label htmlFor="yourname">Your Name</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="email"
                        id="email"
                        className="form-control form-inputbox-bgcolor"
                        name="email1"
                        value={email}
                        onChange={(e) => setemail(e.target.value)}
                        required
                      />
                      <label htmlFor="email">Your Email</label>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-floating">
                      <input
                        type="text"
                        id="subject"
                        className="form-control form-inputbox-bgcolor"
                        name="subject1"
                        value={subject}
                        onChange={(e) => setsubjectName(e.target.value)}
                        required
                      />
                      <label htmlFor="subject">Subject</label>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-floating">
                      <textarea
                        className="form-control form-inputbox-bgcolor"
                        id="message"
                        name="message1"
                        value={message}
                        onChange={(e) => setmessage(e.target.value)}
                        style={{ height: "140px" }}
                        required
                      />
                      <label htmlFor="message">Message</label>
                    </div>
                  </div>
                  <div className="col-12">
                    <button
                      type="submit"
                      className="form-submit-button w-100 py-3"
                    >
                      Send Message
                      <FontAwesomeIcon
                        icon={faArrowRight}
                        className="fa fa-arrow-right ms-3"
                        width={15}
                        height={15}
                      />
                    </button>
                  </div>
                </div>
              </form>
            </div>

            
          </div>
          <div
              className="col-lg-12 col-md-12 wow fadeInUp pb-3"
              data-wow-delay="0.3s"
            >
              <h5>
                <br></br>
              </h5>
              <iframe
                className="position-relative rounded w-100 h-100"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1466376.2810689123!2d80.2027767160663!3d13.052038511339615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a526d00e104e3b7%3A0x9b947e1f43f7e3d0!2sChennai%2C%20Tamil%20Nadu%2C%20India!5e0!3m2!1sen!2sus!4v1605687416428!5m2!1sen!2sus"
                style={{ minHeight: "300px", border: "0" }}
                aria-hidden="false"
                tabIndex={0}
              ></iframe>
            </div>
        </div>
      </div>
      {/* Contact End */}
      
      <Form></Form>
    </>
  );
}
