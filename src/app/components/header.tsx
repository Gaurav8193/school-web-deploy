import { faAngleDown, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image'
import Link from 'next/link';
export default function Header() {
  return (
    <>
    {/* Navbar Start */}
    <nav className="navbar navbar-expand-lg bg-white navbar-light shadow fixed-top p-0">
                <a href="https://mithilaschool.com" className="navbar-brand d-flex align-items-center px-4 px-lg-5"><h2 className="m-0 logo-text-color"><Image src="/logo.jpeg" width={70} height={45} alt="Bootstrap" /><span>Mithila School</span></h2></a>
                <button type="button" className="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse"><span className="navbar-toggler-icon"></span></button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <div className="navbar-nav ms-auto p-4 p-lg-0">
                        <a href="https://mithilaschool.com" className="nav-item nav-link active">Home</a>
                        <a href="/about" className="nav-item nav-link">About</a>
                        <a href="/contact" className="nav-item nav-link">Contact</a>
                        {/* <Link href="/about" className="nav-item nav-link">About</Link>
                        <Link href="/contact" className="nav-item nav-link">Contact</Link> */}
                    </div>
                    <span className="navbar-text m-2">
                        <a href="#demo"><button type="button" className="form-submit-button">Request a Demo<FontAwesomeIcon icon={faArrowRight} className="fa fa-arrow-right ms-3" width={15} height={15} /></button></a>
                    </span>
                    <span className="navbar-text">
                        <a href="https://client.mithilaschool.com"><button type="button" className="css-button-rounded--sky">Get Started<FontAwesomeIcon icon={faArrowRight} className="fa fa-arrow-right ms-3" width={15} height={15} /></button></a>
                    </span>
                </div>
            </nav>
            {/* Navbar End */}
    </>
  );
}
