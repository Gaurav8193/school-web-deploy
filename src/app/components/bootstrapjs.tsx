"use client"
import { useEffect } from "react"

export default function Bootstrapjs(){
    useEffect(()=>{
        require('bootstrap/dist/js/bootstrap.min.js')
    }),
    useEffect(()=>{
        require('bootstrap/dist/css/bootstrap.min.css')
    })
    return (
        null
    )
}
