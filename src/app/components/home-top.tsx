// import React, { useState } from 'react';
import { Carousel } from "react-bootstrap";
import "../globals.css"; // Import the CSS file for styling
import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "react-bootstrap";
import Image from "next/image";
export default function HomeTop() {
  return (
    <>
      {/* <div className="mt-5 pt-4">
        <div className="mt-5 mb-5 text-center" style={{ color: "#004054" }}>
          <h1>Welcome to mithilaschool.com Management System</h1>
          <h3>
            At mithilaschool.com, we strive to provide an efficient and
            comprehensive solution to manage all aspects of school operations.
            Our management system is designed to streamline administrative
            tasks, enhance communication, and support educational excellence.
            Explore our key features:
          </h3>
        </div>
        <Carousel className="CarouselStyle" fade={true}>
          <Carousel.Item>
            <div
              className="d-flex flex-column align-items-center text-white"
              style={{ marginTop: "150px" }}
            >
              <h1>
                With mithilaschool.com, managing your school online is
                effortless.
              </h1>
              <h5>
                Discover how mithilaschool.com makes managing your school’s
                administrative tasks, student records,
              </h5>
              <h5>
                {" "}
                and communication easier than ever. Effortless, intuitive, and
                designed for modern education.
              </h5>
            </div>
          </Carousel.Item>

          <Carousel.Item>
            <div
              className="d-flex flex-column align-items-center text-white"
              style={{ marginTop: "150px" }}
            >
              <h1>Make Data-Driven Decisions</h1>
              <h5>
                Leverage our advanced reporting features to gain insights into
                student performance, attendance, and more. Make informed
                decisions and track progress with ease.
              </h5>
            </div>
          </Carousel.Item>

          <Carousel.Item>
            <div
              className="d-flex flex-column align-items-center text-white"
              style={{ marginTop: "150px" }}
            >
              <h1>Your Data, Safeguarded</h1>
              <h5>
                With top-notch security measures, rest assured that your data is
                protected. Our platform ensures your information remains
                confidential and secure.
              </h5>
            </div>
          </Carousel.Item>
        </Carousel>
      </div>


      <div className="container mt-5">
        <div className="row">
          <div className="col-6">
            <div style={{ marginRight: "5px" }}>
              <img
                className="carouselImage mt-5 img-fluid float-md-start me-3 mb-3 w-80 pb-5"
                src="/feature/module.jpeg"
                alt="Completely Free"
              />
            </div>
            <div>
              <h2>Student Management</h2>
              <p>
                Effortlessly handle student admissions, promotions, and
                terminations. Our system ensures smooth operations with features
                like:
              </p>
              <ul>
                <li>
                  <strong>Admission:</strong> Simplify the enrollment process
                  for new students.
                </li>
                <li>
                  <strong>Promote:</strong> Seamlessly promote students to the
                  next grade level.
                </li>
                <li>
                  <strong>Terminate:</strong> Manage student exits efficiently.
                </li>
                <li>
                  <strong>Notifications:</strong> Stay informed with timely
                  updates.
                </li>
                <li>
                  <strong>Search:</strong> Quickly find student records.
                </li>
                <li>
                  <strong>Track Payments:</strong> Monitor fee payments and
                  dues.
                </li>
              </ul>

              <h2>Attendance Management</h2>
              <p>
                Keep track of student and staff attendance with ease. Our
                attendance module includes:
              </p>
              <ul>
                <li>
                  <strong>Daily Attendance:</strong> Record and monitor daily
                  attendance.
                </li>
                <li>
                  <strong>Attendance Report:</strong> Generate detailed
                  attendance reports.
                </li>
                <li>
                  <strong>Absence Alert:</strong> Receive alerts for unexcused
                  absences.
                </li>
                <li>
                  <strong>Role-Based Access:</strong> Ensure data security with
                  role-specific access.
                </li>
              </ul>
            </div>
          </div>

            <div className="col-6">
              <h2>Fees Management</h2>
              <p>
                Simplify the fee administration process with our robust tools:
              </p>
              <ul>
                <li>
                  <strong>Administrate Fees:</strong> Manage all aspects of fee
                  collection.
                </li>
                <li>
                  <strong>Add / Update Fees:</strong> Easily add or modify fee
                  structures.
                </li>
                <li>
                  <strong>Raise Fees:</strong> Implement fee changes
                  effortlessly.
                </li>
                <li>
                  <strong>Reports, Download, Export:</strong> Generate and
                  export comprehensive fee reports.
                </li>
              </ul>

              <h2>Staff Management</h2>
              <p>Optimize staff management with our dedicated module:</p>
              <ul>
                <li>
                  <strong>Onboarding:</strong> Smooth onboarding process for new
                  staff members.
                </li>
                <li>
                  <strong>Manage Salary:</strong> Efficient salary management
                  and processing.
                </li>
                <li>
                  <strong>Payment & Bonus:</strong> Handle payments and bonuses
                  seamlessly.
                </li>
              </ul>

              <h2>Reports</h2>
              <p>
                Gain valuable insights and keep your accounts in order with our
                reporting tools:
              </p>
              <ul>
                <li>
                  <strong>Account Dashboard:</strong> Get an overview of your
                  financial status.
                </li>
                <li>
                  <strong>Expense Report:</strong> Monitor and manage expenses
                  effectively.
                </li>
                <li>
                  <strong>Earning Report:</strong> Track earnings and financial
                  performance.
                </li>
                <li>
                  <strong>Earning/Expense Forecast:</strong> Plan ahead with
                  accurate financial forecasts.
                </li>
              </ul>
            </div>
        </div>
      </div>

      <img
        className="carouselImage mt-5"
        src="/feature/feature.png"
        alt="Completely Free"
      /> */}
    </>
  );
}

// export default Home;
