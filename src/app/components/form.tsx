"use client"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';

export default function CustomFrom() {
    const [firstName, setfirstName] = useState('')
    const [lastName, setlastName] = useState('')
    const [schoolName, setschoolName] = useState('')
    const [email, setemail] = useState('')
    const [phone, setphone] = useState('')
    const fullName = firstName +" "+ lastName;
    const handleSubmit = async (event: { preventDefault: () => void; }) => {
        // event.preventDefault();
        console.log(fullName+" "+schoolName+" "+email+" "+phone);
        
        fetch("https://api.mithilaschool.com/demo", {
          method: "POST",
          body: JSON.stringify({ fullName, schoolName, email, phone}),
          headers: {
            "content-type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
          },
        }).catch((e) => console.log(e));
        return alert("Thank you! \nYour message has been successfully sent. We will contact you soon!");
    }

    return (
        <>
            <div id="demo" className="form-background pt-5 pb-5 text-white">
                <div className="container">
                    <div className="row">
                        <div className="col-md-4">
                            <h3>Request a Personalized Demo</h3>
                            <p>We all contact you to discuss your specific requirements and arrange your personalized demo.</p>
                        </div>
                        <div className="col-md-2"></div>
                        <div className="col-md-6">
                            <form onSubmit={handleSubmit}>
                                <div className="row mb-4">
                                    <div className="col">
                                        <div data-mdb-input-init className="form-outline">
                                            <label className="form-label" htmlFor="form3Example1">First Name:</label>
                                            <input type="text" id="form3Example1" className="form-control form-inputbox-bgcolor" name='firstName' value={firstName} onChange={(e) => setfirstName(e.target.value)} required />
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div data-mdb-input-init className="form-outline">
                                            <label className="form-label" htmlFor="form3Example2">Last Name:</label>
                                            <input type="text" id="form3Example2" className="form-control form-inputbox-bgcolor" name='lastName' value={lastName} onChange={(e) => setlastName(e.target.value)} />
                                        </div>
                                    </div>
                                </div>
                                <div data-mdb-input-init className="form-outline mb-4">
                                    <label className="form-label" htmlFor="form3Example3">School Name:</label>
                                    <input type="text" id="form3Example3" className="form-control form-inputbox-bgcolor" name='schoolName' value={schoolName} onChange={(e) => setschoolName(e.target.value)} />
                                </div>
                                <div data-mdb-input-init className="form-outline mb-4">
                                    <label className="form-label" htmlFor="form3Example4">Email address:</label>
                                    <input type="email" id="form3Example4" className="form-control form-inputbox-bgcolor" name='email' value={email} onChange={(e) => setemail(e.target.value)} required />
                                </div>
                                <div data-mdb-input-init className="form-outline mb-4">
                                    <label className="form-label" htmlFor="form3Example5">Phone:</label>
                                    <input type="number" id="form3Example5" className="form-control form-inputbox-bgcolor" name='phone' value={phone} onChange={(e) => setphone(e.target.value)} />
                                </div>

                                <span className="navbar-text m-2">
                                    <a href="#demo"><button type='submit' className="form-submit-button">Request a Demo<FontAwesomeIcon icon={faArrowRight} className="fa fa-arrow-right ms-3" width={15} height={15} /></button></a>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}