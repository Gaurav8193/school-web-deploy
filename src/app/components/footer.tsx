import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FaMapMarkerAlt, FaPhoneAlt, FaEnvelope, FaAngleRight } from 'react-icons/fa';
import { faTwitter, faFacebookF, faYoutube, faLinkedinIn, faWhatsapp} from '@fortawesome/free-brands-svg-icons';
import { faArrowRight, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image'
import Link from 'next/link';
export default function Footer(){
    return (
        <>
{/* Footer Start */}
    <div className="container-fluid text-light footer pt-5 wow fadeIn footer-background-color" data-wow-delay="0.1s">
        <div className="container py-5">
            <div className="row g-5">
            <div className="col-lg-3 col-md-6"></div>
                <div className="col-lg-3 col-md-6">
                    <h4 className="text-white mb-3">Contact</h4>
                    <p className="mb-2"><FaMapMarkerAlt className="fa fa-map-marker-alt me-3"/>India, USA</p>
                    <p className="mb-2"><FaPhoneAlt  className="fa fa-phone-alt me-3"/>+91 8210984364</p>
                    <p className="mb-2"><FaEnvelope  className="fa fa-envelope me-3"/>mithilaschoolhelp@gmail.com</p>
                    <div className="d-flex pt-2">
                        {/* <a className="btn btn-outline-light btn-social" href="https://mithilaschool.com"><FontAwesomeIcon className="fab fa-twitter" icon={faTwitter}/></a> */}
                        {/* <a className="btn btn-outline-light btn-social" href="https://mithilaschool.com"><FontAwesomeIcon className="fab fa-facebook-f" icon={faFacebookF }/></a> */}
                        {/* <a className="btn btn-outline-light btn-social" href="https://mithilaschool.com"><FontAwesomeIcon className="fab fa-youtube" icon={faYoutube}/></a> */}
                        {/* <a className="btn btn-outline-light btn-social" href="https://mithilaschool.com"><FontAwesomeIcon className="fab fa-linkedin-in" icon={faLinkedinIn}/></a> */}
                        <a className="btn btn-outline-light btn-social" href="https://chat.whatsapp.com/DdnuXfUq2lJAiQfJhGQR5H"><FontAwesomeIcon className="fab fa-whatsapp-in" icon={faWhatsapp}/></a>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6">
                    <h4 className="text-white mb-3">MITHILA SCHOOL</h4>
                    <p>Innovate Teaching, Promote Skill Building, and Achieve Greater Learning Outcomes!</p>
                    <div className="position-relative mx-auto" style={{maxWidth: "400px"}}>
                    <span className="navbar-text">
                        <a href="https://client.mithilaschool.com"><button type="button" className="css-button-rounded--sky">Get Started<FontAwesomeIcon icon={faArrowRight} className="fa fa-arrow-right ms-3" width={15} height={15} /></button></a>
                    </span>
                    </div>
                </div>
            </div>
        </div>
        <div className="container">
            <div className="copyright">
                <div className="row">
                    <div className="col-md-6 text-center text-md-start mb-3 mb-md-0">
                      <div>© 2024 Copyright : <a href="https://mithilaschool.com" className='text-decoration-none text-info'><b>Mithila School</b></a></div>
                    </div>
                    <div className="col-md-6 text-center text-md-end">
                        <div className="footer-menu">
                            <a className="text-decoration-none" href="/terms">Terms</a>
                            <a className="text-decoration-none" href="/privacy">Privacy</a>
                            {/* <Link href="/components/terms" className="nav-item nav-link">Terms</Link> */}
                            {/* <Link href="/components/privacy" className="nav-item nav-link">Privacy</Link> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {/* Footer End */}
    {/* Back to Top */}
    <a href="#" className="btn-lg-square back-to-top form-submit-button"><i className="bi bi-arrow-up"></i></a>
        </>
    );
}