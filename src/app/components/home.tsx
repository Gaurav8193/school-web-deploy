import "bootstrap-icons/font/bootstrap-icons.css";
import Form from "./form";
import Image from "next/image";
import styles from "./Lightbox.module.css";
import {
  faArrowRight,
  faEnvelope,
  faLocationPin,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { Carousel } from "react-bootstrap";
export default function HomePage() {
  return (
    <>
      <Carousel className="CarouselStyle mt-5 p-4">
        <Carousel.Item>
          <div
            className="d-flex flex-column align-items-center text-white"
            style={{ marginTop: "150px" }}
          >
            <h1 style={{ color: "#ff7a00" }}>
              With mithilaschool.com, managing your school online is effortless.
            </h1>
            <p className="text-style">
              Discover how mithilaschool.com makes managing your school’s
              administrative tasks, student records, and communication easier
              than ever. Effortless, intuitive, and designed for modern
              education.
            </p>
          </div>
        </Carousel.Item>

        <Carousel.Item>
          <div
            className="d-flex flex-column align-items-center text-white"
            style={{ marginTop: "150px" }}
          >
            <h1 style={{ color: "#ff7a00" }}>Make Data-Driven Decisions</h1>
            <p className="text-style">
              Leverage our advanced reporting features to gain insights into
              student performance, attendance, and more. Make informed decisions
              and track progress with ease.
            </p>
          </div>
        </Carousel.Item>

        <Carousel.Item>
          <div
            className="d-flex flex-column align-items-center text-white"
            style={{ marginTop: "150px" }}
          >
            <h1 style={{ color: "#ff7a00" }}>Your Data, Safeguarded</h1>
            <p className="text-style">
              With top-notch security measures, rest assured that your data is
              protected. Our platform ensures your information remains
              confidential and secure.
            </p>
          </div>
        </Carousel.Item>
      </Carousel>
      {/* -------------------------------------------------------------------------------------------------- */}
      <div
        className="container about-page-text"
        style={{
          paddingTop: "3rem !important",
          paddingBottom: "2rem !important",
        }}
      >
        <div className="row">
          {/* <div className="col"></div> */}
          <div className="col-12 fs-4 text-center">
            {/* <h2 style={{ color: "#ff7a00" }}>Welcome to mithilaschool.com Management System</h2> */}
            <p className="text-style">
              At mithilaschool.com, we strive to provide an efficient and
              comprehensive solution to manage all aspects of school operations.
              Our management system is designed to streamline administrative
              tasks, enhance communication, and support educational excellence.
              Explore our key features:
            </p>
          </div>
          {/* <div className="col"></div> */}
        </div>
      </div>
      {/* -------------------------------------------------------------------------------------------------- */}
      <div className="container moduleContainer">
      <div className="row align-items-center flex-column-reverse flex-md-row">
        <div className="col-md-6">
        <h4>Student Management</h4>
              <p>
                Effortlessly handle student admissions, promotions, and
                terminations. Our system ensures smooth operations with features
                like:
              </p>
              <ul>
                <li>
                  <strong>Admission:</strong> Simplify the enrollment process
                  for new students.
                </li>
                <li>
                  <strong>Promote:</strong> Seamlessly promote students to the
                  next grade level.
                </li>
                <li>
                  <strong>Terminate:</strong> Manage student exits efficiently.
                </li>
                <li>
                  <strong>Notifications:</strong> Stay informed with timely
                  updates.
                </li>
                <li>
                  <strong>Search:</strong> Quickly find student records.
                </li>
                <li>
                  <strong>Track Payments:</strong> Monitor fee payments and
                  dues.
                </li>
              </ul>
        </div>
        <div className="col-md-6 text-center">
          <Image
            src="/homepage/studentManagement.jpg"
            alt="Descriptive Alt Text"
            width={500}
            height={500}
            className="image"
          />
        </div>
      </div>
    </div>


    <div className="container moduleContainer">
      <div className="row align-items-center">
      <div className="col-md-6 text-center">
          <Image
            src="/homepage/fees.jpg"
            alt="Descriptive Alt Text"
            width={500}
            height={500}
            className="image"
          />
        </div>
        <div className="col-md-6">
        <h4>Fees Management</h4>
              <p>
                Simplify the fee administration process with our robust tools:
              </p>
              <ul>
                <li>
                  <strong>Administrate Fees:</strong> Manage all aspects of fee
                  collection.
                </li>
                <li>
                  <strong>Add / Update Fees:</strong> Easily add or modify fee
                  structures.
                </li>
                <li>
                  <strong>Raise Fees:</strong> Implement fee changes
                  effortlessly.
                </li>
                <li>
                  <strong>Reports, Download, Export:</strong> Generate and
                  export comprehensive fee reports.
                </li>
              </ul>
        </div>
      </div>
    </div>



    <div className="container moduleContainer">
      <div className="row align-items-center flex-column-reverse flex-md-row">
        <div className="col-md-6">
        <h4>Attendance Management</h4>
              <p>
                Keep track of student and staff attendance with ease. Our
                attendance module includes:
              </p>
              <ul>
                <li>
                  <strong>Daily Attendance:</strong> Record and monitor daily
                  attendance.
                </li>
                <li>
                  <strong>Attendance Report:</strong> Generate detailed
                  attendance reports.
                </li>
                <li>
                  <strong>Absence Alert:</strong> Receive alerts for unexcused
                  absences.
                </li>
                <li>
                  <strong>Role-Based Access:</strong> Ensure data security with
                  role-specific access.
                </li>
              </ul>
        </div>
        <div className="col-md-6 text-center">
          <Image
            src="/homepage/attendance.jpg"
            alt="Descriptive Alt Text"
            width={500}
            height={500}
            className="image"
          />
        </div>
      </div>
    </div>


    <div className="container moduleContainer">
      <div className="row align-items-center">
      <div className="col-md-6 text-center">
          <Image
            src="/homepage/staff.jpg"
            alt="Descriptive Alt Text"
            width={500}
            height={500}
            className="image"
          />
        </div>
        <div className="col-md-6">
        <h4>Staff Management</h4>
              <p>Optimize staff management with our dedicated module:</p>
              <ul>
                <li>
                  <strong>Onboarding:</strong> Smooth onboarding process for new
                  staff members.
                </li>
                <li>
                  <strong>Manage Salary:</strong> Efficient salary management
                  and processing.
                </li>
                <li>
                  <strong>Payment & Bonus:</strong> Handle payments and bonuses
                  seamlessly.
                </li>
              </ul>
        </div>
      </div>
    </div>


    <div className="container moduleContainer">
      <div className="row align-items-center flex-column-reverse flex-md-row">
        <div className="col-md-6">
        <h4>Reports</h4>
              <p>
                Gain valuable insights and keep your accounts in order with our
                reporting tools:
              </p>
              <ul>
                <li>
                  <strong>Account Dashboard:</strong> Get an overview of your
                  financial status.
                </li>
                <li>
                  <strong>Expense Report:</strong> Monitor and manage expenses
                  effectively.
                </li>
                <li>
                  <strong>Earning Report:</strong> Track earnings and financial
                  performance.
                </li>
                <li>
                  <strong>Earning/Expense Forecast:</strong> Plan ahead with
                  accurate financial forecasts.
                </li>
              </ul>
        </div>
        <div className="col-md-6 text-center">
          <Image
            src="/homepage/reports.jpg"
            alt="Descriptive Alt Text"
            width={500}
            height={500}
            className="image"
          />
        </div>
      </div>
    </div>
      {/* --------------------------------------------------------------------------------------------------------- */}
      {/* About Start */}
      {/* <div className="container-xxl py-3">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 wow fadeInUp">
              {" "}
              <div>
                <Image
                  src="/feature/module.jpeg"
                  className="img-fluid position-relative mb-5"
                  width={664}
                  height={664}
                  alt="..."
                />{" "}
              </div>
              <div>
                <h4>Student Management</h4>
                <p>
                  Effortlessly handle student admissions, promotions, and
                  terminations. Our system ensures smooth operations with
                  features like:<br></br>
                  <strong className="about-page-text">Admission:</strong>{" "}
                  Simplify the enrollment process for new students.<br></br>
                  <strong className="about-page-text">Promote:</strong>{" "}
                  Seamlessly promote students to the next grade level.<br></br>
                  <strong className="about-page-text">Terminate:</strong> Manage
                  student exits efficiently.<br></br>
                  <strong className="about-page-text">
                    Notifications:
                  </strong>{" "}
                  Stay informed with timely updates.<br></br>
                  <strong className="about-page-text">Search:</strong> Quickly
                  find student records.<br></br>
                  <strong className="about-page-text">
                    Track Payments:
                  </strong>{" "}
                  Monitor fee payments and dues.
                </p>

                <h4>Staff Management</h4>
                <p>
                  Optimize staff management with our dedicated module:<br></br>
                  <strong className="about-page-text">Onboarding:</strong>{" "}
                  Smooth onboarding process for new staff members.<br></br>
                  <strong className="about-page-text">
                    Manage Salary:
                  </strong>{" "}
                  Efficient salary management and processing.<br></br>
                  <strong className="about-page-text">
                    Payment & Bonus:
                  </strong>{" "}
                  Handle payments and bonuses seamlessly.
                </p>
              </div>
            </div>
            <div className="col-lg-6 wow fadeInUp" data-wow-delay="0.3s">
              <h4>Fees Management</h4>
              <p>
                Simplify the fee administration process with our robust tools:
                <br></br>
                <strong className="about-page-text">
                  Administrate Fees:
                </strong>{" "}
                Manage all aspects of fee collection.<br></br>
                <strong className="about-page-text">
                  Add / Update Fees:
                </strong>{" "}
                Easily add or modify fee structures.<br></br>
                <strong className="about-page-text">Raise Fees:</strong>{" "}
                Implement fee changes effortlessly.<br></br>
                <strong className="about-page-text">
                  Reports, Download, Export:
                </strong>{" "}
                Generate and export comprehensive fee reports.
              </p>

              <h4>Attendance Management</h4>
              <p>
                Keep track of student and staff attendance with ease. Our
                attendance module includes:<br></br>
                <strong className="about-page-text">
                  Daily Attendance:
                </strong>{" "}
                Record and monitor daily attendance.<br></br>
                <strong className="about-page-text">
                  Attendance Report:
                </strong>{" "}
                Generate detailed attendance reports.<br></br>
                <strong className="about-page-text">Absence Alert:</strong>{" "}
                Receive alerts for unexcused absences.<br></br>
                <strong className="about-page-text">
                  Role-Based Access:
                </strong>{" "}
                Ensure data security with role-specific access.
              </p>

              <h4>Reports</h4>
              <p>
                Gain valuable insights and keep your accounts in order with our
                reporting tools:<br></br>
                <strong className="about-page-text">
                  Account Dashboard:
                </strong>{" "}
                Get an overview of your financial status.<br></br>
                <strong className="about-page-text">
                  Expense Report:
                </strong>{" "}
                Monitor and manage expenses effectively.<br></br>
                <strong className="about-page-text">
                  Earning Report:
                </strong>{" "}
                Track earnings and financial performance.<br></br>
                <strong className="about-page-text">
                  Earning/Expense Forecast:
                </strong>{" "}
                Plan ahead with accurate financial forecasts.
              </p>
              <span className="navbar-text">
                <a href="#demo">
                  <button type="button" className="form-submit-button">
                    Request a Demo
                    <FontAwesomeIcon
                      icon={faArrowRight}
                      className="fa fa-arrow-right ms-3"
                      width={15}
                      height={15}
                    />
                  </button>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div> */}
      {/* About End */}
      {/* ---------------------------------------------------------------------------------------------- */}
      <div className="text-center">
        <Image
          src="/feature/feature.png"
          className="img-fluid"
          width={1200}
          height={100}
          alt="..."
        />
      </div>

      {/* ---------------------------------------------------------------------------- */}
      {/* Contact Start */}
      <div className="text-center wow fadeInUp mt-5" data-wow-delay="0.1s">
        <h6 className="section-title bg-white text-center text-color px-3">
          Contact Us
        </h6>
        <h3>Contact for any query</h3>
      </div>
      <div className="container py-3 pb-5">
        <div className="row g-4 justify-content-center">
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  icon={faEnvelope}
                  className="icon-responsive"
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">E-mail</h5>
                <p>mithilaschoolhelp@gmail.com</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  icon={faPhone}
                  className="icon-responsive"
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">Phone</h5>
                <p>+91 7250410621</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.7s">
            <div className="service-item text-center pt-1" style={{borderRadius: "20px"}}>
              <div className="p-4">
                <FontAwesomeIcon
                  className="fab fa-whatsapp-in"
                  icon={faWhatsapp}
                  width={25}
                  height={25}
                  color="#06BBCC"
                />
                <h5 className="mb-3">WhatsApp Group</h5>
                <p>
                  <a
                    className="link-dark"
                    href="https://chat.whatsapp.com/DdnuXfUq2lJAiQfJhGQR5H"
                  >
                    Join Now
                  </a>
                </p>
              </div>
            </div>
          </div>
          {/* <div
              className="col-lg-3 col-sm-6 wow fadeInUp"
              data-wow-delay="0.5s"
            >
              <div className="service-item text-center pt-1">
                <div className="p-4">
                  <FontAwesomeIcon
                    icon={faLocationPin}
                    className="icon-responsive"
                    width={40}
                    height={40}
                    color="#06BBCC"
                  />
                  <h5 className="mb-3">Location</h5>
                  <p>Chennai India | USA</p>
                </div>
              </div>
            </div> */}
        </div>
      </div>
      {/* </div> */}
      {/* Contact End */}
      <Form></Form>
    </>
  );
}
